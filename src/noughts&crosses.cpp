#include "noughts&crosses.hpp"
#include <iostream>

void draw_board(float size_square, p6::Context& ctx, p6::Color color)
{
    ctx.background({0, 0, 0}); // Clear the background with some color (Try to comment out this line to see what happens)
    ctx.use_fill   = true;
    ctx.use_stroke = false;
    ctx.fill       = color;
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            ctx.square(p6::Center{j * (1 - size_square), i * (1 - size_square)}, p6::Radius{size_square});
        }
    }
}

void draw_circle(CellIndex cell, p6::Context& ctx, float size)
{
    ctx.use_fill      = false;
    ctx.use_stroke    = true;
    ctx.stroke        = {0, 0, 0};
    ctx.stroke_weight = 0.1f;
    ctx.circle(p6::Center{(cell.x - 1) * (1 - size), (cell.y - 1) * (1 - size)}, p6::Radius{0.28f});
}

float bottom_left_cross(float coord, float size)
{
    return (coord - 1.0f) * (1 - size) - size;
}

float top_right_cross(float coord, float size)
{
    return (coord - 3 * size / 2.0f) * (1 - size);
}

void draw_cross(CellIndex cell, p6::Context& ctx, float size)
{
    ctx.stroke        = {0, 0, 0};
    ctx.stroke_weight = 0.02f;
    ctx.line(glm::vec2{bottom_left_cross(cell.x, size), bottom_left_cross(cell.y, size)},
             glm::vec2{top_right_cross(cell.x, size), top_right_cross(cell.y, size)});
    ctx.line(glm::vec2{top_right_cross(cell.x, size), bottom_left_cross(cell.y, size)},
             glm::vec2{bottom_left_cross(cell.x, size), top_right_cross(cell.y, size)});
}

glm::vec2 cell_index_to_position(CellIndex cell)
{
    glm::vec2 res(cell.x, cell.y);
    return res;
}

int check_position(float coord)
{
    if (coord <= -1 / 3.0) {
        return 0;
    }
    else if (coord > 1 / 3.0) {
        return 2;
    }
    else {
        return 1;
    }
}

CellIndex position_to_cell_index(glm::vec2 mouse_vec)
{
    CellIndex res;
    //On convertit l'abscisse en entier entre 0 et 2 sachant que la grille fait 3x3
    res.x = check_position(mouse_vec[0]);
    //Idem pour l'ordonnée
    res.y = check_position(mouse_vec[1]);
    return res;
}

template<int size>
void draw_noughts_and_crosses(const Board<size>& board, p6::Context& ctx)
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            auto cell = board[{i, j}];
            if (cell.has_value()) {
                if (*cell == Player::Crosses) {
                    draw_cross({i, j}, ctx, 0.32f);
                }
                else {
                    draw_circle({i, j}, ctx, 0.32f);
                }
            }
        }
    }
}

void swap_player(Player& player)
{
    if (player == Player::Crosses) {
        player = Player::Noughts;
    }
    else {
        player = Player::Crosses;
    }
}

template<int size>
void make_a_move(CellIndex cell, Board<size>& board, Player& current_player)
{
    auto empty_cell = !board[cell].has_value();
    if (empty_cell) {
        board[cell] = current_player;
        swap_player(current_player);
    }
}

template<int size>
void preview_placement(Board<size>& board, p6::Context& ctx, Player& current_player)
{
    CellIndex cell  = position_to_cell_index(ctx.mouse());
    auto      coord = board[cell];
    if (!coord.has_value()) {
        if (current_player == Player::Crosses) {
            draw_cross(cell, ctx, 0.32f);
        }
        else {
            draw_circle(cell, ctx, 0.32f);
        }
    }
}

template<int size>
bool game_over(Board<size>& board)
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            auto cell = board[{i, j}];
            if (!cell.has_value()) {
                return false;
            }
        }
    }
    return true;
}

template<int size>
std::optional<Player> is_there_a_winner(Board<size>& board)
{
    for (int i = 0; i < size; i++) {
        //Check the rows
        if (board[{i, 0}].has_value() && board[{i, 0}] == board[{i, 1}] && board[{i, 1}] == board[{i, 2}]) {
            return board[{i, 0}];
        }
        //Check the columns
        if (board[{0, i}].has_value() && board[{0, i}] == board[{1, i}] && board[{1, i}] == board[{2, i}]) {
            return board[{0, i}];
        }
    }
    //Check the diagonals
    if (board[{1, 1}].has_value() && ((board[{0, 0}] == board[{1, 1}] && board[{1, 1}] == board[{2, 2}]) || (board[{0, 2}] == board[{1, 1}] && board[{1, 1}] == board[{2, 0}]))) {
        return board[{1, 1}];
    }
    return Player::Empty;
}

void play_noughts_crosses()
{
    Board<3>  board;
    auto      current_player = Player::Crosses;
    auto      ctx            = p6::Context{{720, 720, "Noughts & Crosses"}}; // Create a context with a window
    auto      size_square    = 0.32f;
    p6::Color color{1.f, 0.7f, 0.2f};
    ctx.update = [&]() { // Define the function that will be called in a loop once you call ctx.start()
        draw_board(size_square, ctx, color);
        draw_noughts_and_crosses<3>(board, ctx);
        preview_placement(board, ctx, current_player);

    };

    ctx.mouse_pressed = [&](p6::MouseButton event) {
        if (!game_over(board)) {
            if (is_there_a_winner(board) == Player::Empty) {
                make_a_move(position_to_cell_index(event.position), board, current_player);
            }
            else {
                if (is_there_a_winner(board) == Player::Crosses) {
                    std::cout << "The winner is: Crosses" << std::endl;
                }
                else {
                    std::cout << "The winner is: Noughts" << std::endl;
                }
            }
        }
    };

    ctx.start(); // Start the p6 application
}