#pragma once

#include <p6/p6.h>

struct CellIndex {
    int x;
    int y;
};

enum class Player {
    Noughts,
    Crosses,
    Empty,
};

template<int size>
class Board {
public:
    std::optional<Player>& operator[](CellIndex index)
    {
        return _cells[index.x][index.y];
    }

    const std::optional<Player>& operator[](CellIndex index) const
    {
        return _cells[index.x][index.y];
    }

private:
    std::array<std::array<std::optional<Player>, size>, size> _cells;
};

void draw_board(float size_square, p6::Context& ctx, p6::Color color);

void draw_circle(CellIndex cell, p6::Context& ctx, float size);

float bottom_left_cross(float coord, float size);

float top_right_cross(float coord, float size);

void draw_cross(CellIndex cell, p6::Context& ctx, float size);

glm::vec2 cell_index_to_position(CellIndex cell);

int check_position(float coord);

CellIndex position_to_cell_index(glm::vec2 mouse_vec);

template<int size>
void draw_noughts_and_crosses(const Board<size>& board, p6::Context& ctx);

void swap_player(Player& player);

template<int size>
void make_a_move(CellIndex cell, Board<size>& board, Player& current_player);

template<int size>
void preview_placement(Board<size>& board, p6::Context& ctx, Player& current_player);

template<int size>
bool game_over(Board<size>& board);

template<int size>
std::optional<Player> is_there_a_winner(Board<size>& board);

void play_noughts_crosses();