# Avancement en prog-objet

## 9 février:

### Level 1:

    Install a compiler
    Use an IDE
    Use a debugger
    Use a formatting tool
    Use static analysers
    Naming

## 16 février:

### Level 1:
    Stack vs Heap
    Consistency in style

### Level 2:
    Make it work, then make it good

### Assignement:
    Introduction
    Setting up a project
    Guess the number
    Hangman (en cours)

## 23 février:

### Level 4:
    std::string and std:string_view

### Assignement:
    Hangman (almost done)

## 2 mars:

### Level 2:
    Prefer free functions
    Design cohesive classes
    Use structs to group data
    Write small functions
    DRY: Don't repeat yourself
    Enums
    Split problems in small pieces

## Assignement:
    Hangman (works but the code can be improved on)

## 9 mars:

## Assignement:
    Hangman (bonus)

## 16 mars:

## Assignement:
    Hangman (finished bonus)
    Menu

## 23 mars:

### Level 3:
    std::vector
    Documentation
    Use libraries
    assert
    auto
    CMake
    Space out your code

## Assignement: 
    Hangman (fix DRY)
    Menu (refactoring)

